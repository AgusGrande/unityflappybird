using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class m_guiScore : MonoBehaviour
{
    [SerializeField]
    private m_birdController m_bird;
    private TMPro.TextMeshProUGUI m_TextScore;

    private void Awake()
    {
        m_TextScore = GetComponent<TMPro.TextMeshProUGUI>();
    }

    private void Update()
    {
        m_TextScore.text = m_bird.getscore().ToString();
    }


}
