using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class m_pipesSpawner : MonoBehaviour
{

    [SerializeField]
    private GameObject m_PipeBottom;

    [SerializeField]
    private GameObject m_PipeTop;

    [SerializeField]
    private GameObject m_PipeFlag;

    [SerializeField]
    private float m_SpawnRateEasy = 3f;
    [SerializeField]
    private float m_SpawnRateMedium = 2f;
    [SerializeField]
    private float m_SpawnRateHard = 1.2f;

    private int m_cycles = 0;

    public Coroutine spawnCoroutine;

    void Start()
    {
        spawnCoroutine = StartCoroutine(SpawnCoroutine());
        //StartCoroutine(IncreaseDifficulty());
    }


    public void StopSpawn() => StopCoroutine(spawnCoroutine);

    IEnumerator SpawnCoroutine()
    {
        yield return new WaitForSeconds(4);
        while (true)
        {
            GameObject spawnedTop = Instantiate(m_PipeTop);
            GameObject spawnedBot = Instantiate(m_PipeBottom);
            GameObject spawnedFlag = Instantiate(m_PipeFlag);
            float randomNumber = Random.Range(2, 6);

            spawnedTop.transform.position = new Vector2(4, randomNumber);
            spawnedBot.transform.position = new Vector2(4, (randomNumber-8));
            spawnedFlag.transform.position = new Vector2(5,0);

            spawnedTop.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-3, 0); ;
            spawnedBot.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-3, 0); ;
            spawnedFlag.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-3, 0); ;
            m_cycles++;
            if (m_cycles < 6)
            {
                yield return new WaitForSeconds(m_SpawnRateEasy);
            }else if (m_cycles < 12)
            {
                yield return new WaitForSeconds(m_SpawnRateMedium);
            }
            else
            {
                yield return new WaitForSeconds(m_SpawnRateHard);
            }
        }
    }
}
