using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class m_birdController : MonoBehaviour
{
    [SerializeField]
    private float m_JumpSpeed = 5f;

    [SerializeField]
    public m_ManagerScript m_manager;

    private Rigidbody2D m_rigidBody;

    private int m_score = 0;

    public string getscore()  
    {
        return m_score.ToString();
    }



    void Awake()
    {
        print("Bird Created");
        m_rigidBody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.S))
        {
            //Do not use velocity, use addforce instead :)
            //m_rigidBody.velocity = new Vector3(m_rigidBody.velocity.x,m_JumpSpeed,0);
            m_rigidBody.AddForce(Vector2.up * m_JumpSpeed, ForceMode2D.Impulse);
        }
        transform.eulerAngles = new Vector3(0, 0, m_rigidBody.velocity.y*3);

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "m_PipeFlag")
        {
            m_score++;
            if (m_score % 2==0)
            {
                m_manager.changeBackground();
            }
        }
        else
        {
            print("GAME OVER, TRIGGER: COLLIDED WITH PIPE NAME -> " + collision.gameObject);
            Destroy(gameObject);
            m_manager.gameOver();
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {  
            Destroy(gameObject);
            m_manager.gameOver();
    }
}
