using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class m_ManagerScript : MonoBehaviour
{
    [SerializeField]
    public GameObject m_canvasUlost;

    [SerializeField]
    private GameObject m_day;
    [SerializeField]
    private GameObject m_night;


    public void gameOver()
    {
        m_canvasUlost.SetActive(true);
        Time.timeScale = 0;
    }
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1;
        m_canvasUlost.SetActive(false);
    }

    public void Restart()
    {
        SceneManager.LoadScene("GameScene");
    }
    public  void changeBackground()
    {
        if (m_day.active)
        {
            m_day.SetActive(false);
            m_night.SetActive(true);
        }
        else
        {
            m_day.SetActive(true);
            m_night.SetActive(false);
        }
    }

    
}
